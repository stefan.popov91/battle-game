﻿using BattleGame.Domain.Core;
using BattleGame.Infrastructure.Data;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using BattleGame.Domain.Services.Armies;

namespace BattleGame.Api.Controllers
{
    [Route("[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ArmyController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public ArmyController(IMediator mediator, IUnitOfWork unitOfWork)
        {
            _mediator = mediator;
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<CommandEmptyResult>> Add(AddArmyCommand command)
        {
            var result = await _mediator.Send(command);
            if (result.IsSuccess)
            {
                _unitOfWork.SaveChanges();
                return Ok(result.Payload);
            }

            return BadRequest(result.FailureReason);
        }
    }
}
