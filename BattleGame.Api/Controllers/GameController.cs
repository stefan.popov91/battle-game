﻿using System;
using System.Collections.Generic;
using BattleGame.Domain.Services.Games;
using BattleGame.Infrastructure.Data;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BattleGame.Api.Controllers
{
    [Route("[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class GameController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;

        public GameController(IMediator mediator, IUnitOfWork unitOfWork)
        {
            _mediator = mediator;
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("start")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<string>> Start()
        {
            var result = await _mediator.Send(new StartGameCommand());
            if (result.IsSuccess)
            {
                return Ok(result.Payload);
            }

            return BadRequest(result.FailureReason);
        }

        [HttpGet]
        [Route("log/{gameId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ICollection<string>>> GameLog(Guid gameId)
        {
            var result = await _mediator.Send(new RetrieveGameLogCommand{GameId = gameId});
            if (result.IsSuccess)
            {
                return Ok(result.Payload);
            }

            return BadRequest(result.FailureReason);
        }

        [HttpPost]
        [Route("reset")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<string>> ResetActiveGame()
        {
            var result = await _mediator.Send(new ResetActiveGameCommand());
            if (result.IsSuccess)
            {
                return Ok(result.Payload);
            }

            return BadRequest(result.FailureReason);
        }
    }
}
