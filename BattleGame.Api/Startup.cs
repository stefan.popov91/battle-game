﻿using Autofac;
using BattleGame.Domain.Services;
using BattleGame.Infrastructure.Autofac;
using BattleGame.Infrastructure.Data.Ef;
using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace BattleGame.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "BattleGame.Api", Version = "v1" }); });

            services.AddCors();
            services.AddDbContext<BattleGameContext>();
            services.AddMediatR(
                typeof(CommandResult<>).Assembly
            );


            services.AddHangfire(configuration => configuration.UseSqlServerStorage(
            Configuration.GetConnectionString("BattleGameDbContext")));

            services.AddHangfireServer();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, BattleSimulator battleSimulator)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BattleGame.Api");
                c.RoutePrefix = String.Empty;
            });

            app.UseHttpsRedirection();
            app.UseMvc();

            GlobalConfiguration.Configuration
                .UseActivator(new HangfireActivator(serviceProvider));

            app.UseHangfireServer();
            app.UseHangfireDashboard();
            BackgroundJob.Enqueue(() => battleSimulator.ContinueGame());
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<EntityFrameworkModule>();
            builder.RegisterModule<ServicesModule>();
        }
    }
}
