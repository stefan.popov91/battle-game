﻿using System;

namespace BattleGame.Domain.Core
{
    public static class Probability
    {
        public static bool IsSuccessful(double percentage)
        {
            var random = new Random();

            return random.NextDouble() < percentage;
        }
    }
}
