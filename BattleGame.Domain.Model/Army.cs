﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleGame.Domain.Core;

namespace BattleGame.Domain.Model
{
    public class Army : Entity
    {
        public string Name { get; set; }
        public AttackStrategy AttackStrategy { get; set; }
        public Guid GameId { get; set; }
        public Game Game { get; set; }
        public int Units { get; set; }

        public ICollection<Event> Events { get; set; }
        public int TotalAttackDamage => (int)(UnitDamage * Units);

        public bool ReadyToAttack => NextAttackIn == 0;

        public decimal NextAttackIn { get; set; }

        private double UnitDamage => 0.5;
        private double ChanceOfSuccessPerUnit => 1;
        private decimal ReloadTime => (decimal)0.01;
        public double AttackSuccessRate => ChanceOfSuccessPerUnit * Units / 100;


        public void Initialize()
        {
            Events.OrderBy(x => x.GameTime).ToList().ForEach(x =>
            {
                switch (x.EventType)
                {
                    case EventType.ArmyCreated:
                    case EventType.ArmyReset:
                        Units = int.Parse(x.Value);
                        NextAttackIn = Units * ReloadTime;
                        break;
                    case EventType.ArmyReload:
                        NextAttackIn = decimal.Parse(x.Value);
                        break;
                    case EventType.UnitsKilled:
                        Units -= int.Parse(x.Value);
                        break;
                }
            });
        }


        public void DefendAgainst(Army attackingArmy, TimeSpan gameTime)
        {
            if (Probability.IsSuccessful(attackingArmy.AttackSuccessRate))
            {
                Units = Math.Max(Units - attackingArmy.TotalAttackDamage, 0);
                Events.Add(new Event
                {
                    Id = Guid.NewGuid(),
                    EventType = EventType.UnitsKilled,
                    Value = attackingArmy.TotalAttackDamage.ToString(),
                    GameTime = gameTime
                });
                var timeDecrease = attackingArmy.TotalAttackDamage * ReloadTime;
                NextAttackIn = Math.Max(NextAttackIn - timeDecrease, 0);
                Events.Add(new Event
                {
                    Id = Guid.NewGuid(),
                    EventType = EventType.ArmyReload,
                    Value = NextAttackIn.ToString(),
                    GameTime = gameTime
                });

            }

            attackingArmy.ReloadArmy(gameTime);
        }

        public void ReloadArmy(TimeSpan gameTime)
        {
            NextAttackIn = ReloadTime * Units;
            Events.Add(new Event
            {
                Id = Guid.NewGuid(),
                EventType = EventType.ArmyReload,
                Value = NextAttackIn.ToString(),
                GameTime = gameTime
            });
        }

        public void DecreaseTime(decimal timePassed)
        {
            NextAttackIn = Math.Max(NextAttackIn - timePassed, 0);
        }

        public void Reset(TimeSpan gameTime)
        {
            var initialEvent = Events.Single(x => x.EventType == EventType.ArmyCreated);
            Events.Add(new Event
            {
                GameTime = gameTime,
                EventType = EventType.ArmyReset,
                Id = Guid.NewGuid(),
                Value = initialEvent.Value
            });
            Units = int.Parse(initialEvent.Value);
            NextAttackIn = NextAttackIn = Units * ReloadTime;
        }
    }
}
