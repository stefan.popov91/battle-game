﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleGame.Domain.Model
{
    public static class ArmyExtensions
    {
        public static void SetTimePassed(this IEnumerable<Army> query, decimal timePassed, TimeSpan gameTime)
        {
            query.ToList().ForEach(x =>
            {
                x.DecreaseTime(timePassed);
                x.Events.Add(new Event
                {
                    GameTime = gameTime,
                    EventType = EventType.ArmyReload,
                    Id = Guid.NewGuid(),
                    Value = x.NextAttackIn.ToString()
                });
            });
        }

        public static IEnumerable<Army> ArmiesReadyToAttack(this IEnumerable<Army> query)
        {
            return query.Where(x => x.ReadyToAttack);
        }

        public static IEnumerable<Army> Alive(this IEnumerable<Army> query)
        {
            return query.Where(x => x.Units > 0);
        }

        public static Army GetWeakestArmy(this IEnumerable<Army> query)
        {
            var q = query.ToList();
            return q.ToList().First(x => x.Units == q.ToList().Min(z => z.Units));
        }

        public static Army GetStrongestArmy(this IEnumerable<Army> query)
        {
            var q = query.ToList();
            return q.ToList().First(x => x.Units == q.ToList().Max(z => z.Units));
        }

        public static Army GetRandomArmy(this IEnumerable<Army> query)
        {
            var q = query.ToList();
            var random = new Random();
            var index = random.Next(0, q.Count - 1);

            return q[index];
        }
    }
}
