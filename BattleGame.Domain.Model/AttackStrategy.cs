﻿namespace BattleGame.Domain.Model
{
    public enum AttackStrategy
    {
        Random = 1,
        Weakest = 2,
        Strongest = 3
    }
}
