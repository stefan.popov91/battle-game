﻿using System;

namespace BattleGame.Domain.Model
{
    public class Event : Entity
    {
        public Guid ArmyId { get; set; }
        public EventType EventType { get; set; }
        public string Value { get; set; }
        public TimeSpan GameTime { get; set; }
        public Army Army { get; set; }

        public string LogEntry => $"{GameTime}: {Description()}";

        private string Description()
        {
            switch (EventType)
            {
                case EventType.ArmyCreated:
                    return $"Army {Army.Name} was created. Starting units: {Value}";
                case EventType.ArmyReload:
                    return $"Army {Army.Name} reload started. Next attack in {Value}";
                case EventType.UnitsKilled:
                    return $"Army {Army.Name} lost {Value} units";
                case EventType.ArmyReset:
                    return $"Army {Army.Name} size reset to {Value} units";
            }

            return "";
        }
    }
}
