﻿namespace BattleGame.Domain.Model
{
    public enum EventType
    {
        ArmyCreated = 1,
        ArmyReload = 2,
        UnitsKilled = 3,
        ArmyReset = 4
    }
}
