﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleGame.Domain.Model
{
    public class Game : Entity
    {
        public GameStatus GameStatus { get; set; }
        public ICollection<Army> Armies { get; set; }

        public void Initialize()
        {
            Armies.ToList().ForEach(x =>
            {
                x.Initialize();
            });
        }
        public decimal NextAttackIn => Armies.Alive().Min(x => x.NextAttackIn);

        public void NextTurn(decimal timePassed, TimeSpan gameTime)
        {
            var aliveArmies = Armies.Alive().ToList();
            aliveArmies.SetTimePassed(timePassed, gameTime);
            aliveArmies.ArmiesReadyToAttack().ToList().ForEach(x =>
            {
                var remainingArmies = Armies.Where(a => a.Id != x.Id);
                switch (x.AttackStrategy)
                {
                    case AttackStrategy.Weakest:
                        remainingArmies.GetWeakestArmy().DefendAgainst(x, gameTime);
                        break;
                    case AttackStrategy.Strongest:
                        remainingArmies.GetStrongestArmy().DefendAgainst(x, gameTime);
                        break;
                    case AttackStrategy.Random:
                        remainingArmies.GetRandomArmy().DefendAgainst(x, gameTime);
                        break;
                }
            });
        }

        public void Reset(TimeSpan gameTime)
        {
            Armies.ToList().ForEach(x => x.Reset(gameTime));
        }

    }
}
