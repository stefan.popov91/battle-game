﻿using System.Collections.Generic;
using System.Linq;

namespace BattleGame.Domain.Model
{
    public static class GameExtensions
    {
        public static Game GetActiveGame(this IQueryable<Game> query)
        {
            return query.SingleOrDefault(x => x.GameStatus == GameStatus.InProgress || x.GameStatus == GameStatus.Initializing);
        }

        public static IEnumerable<Event> GetAllEvents(this Game game)
        {
            return game.Armies.SelectMany(x => x.Events).OrderBy(x => x.GameTime).ThenByDescending(x => x.EventType);
        }
    }
}
