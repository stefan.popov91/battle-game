﻿namespace BattleGame.Domain.Model
{
    public enum GameStatus
    {
        Initializing = 1,
        InProgress = 2,
        Finished = 3
    }
}
