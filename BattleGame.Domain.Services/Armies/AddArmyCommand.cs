﻿using System.ComponentModel.DataAnnotations;
using BattleGame.Domain.Core;
using BattleGame.Domain.Model;
using MediatR;

namespace BattleGame.Domain.Services.Armies
{
    public class AddArmyCommand : IRequest<CommandResult<CommandEmptyResult>>
    {
        [Required]
        [MinLength(1)]
        public string Name { get; set; }
        [Required]
        public AttackStrategy AttackStrategy { get; set; }
        [Required]
        [Range(80, 100)]
        public int Units { get; set; }
    }
}
