﻿using BattleGame.Domain.Core;
using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BattleGame.Domain.Services.Armies
{
    public class AddArmyCommandHandler : IRequestHandler<AddArmyCommand, CommandResult<CommandEmptyResult>>
    {
        private readonly IRepository<Game> _gameRepository;

        public AddArmyCommandHandler(
            IRepository<Game> gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public Task<CommandResult<CommandEmptyResult>> Handle(AddArmyCommand request, CancellationToken cancellationToken)
        {
            var activeGame = _gameRepository.QueryAll()
                .Include(x => x.Armies)
                .ThenInclude(x => x.Events).GetActiveGame();
            if (activeGame == null)
            {
                var game = new Game
                {
                    GameStatus = GameStatus.Initializing,
                    Id = Guid.NewGuid(),
                    Armies = new List<Army>()
                };
                _gameRepository.Add(game);
                activeGame = game;
            }

            if (activeGame.GameStatus == GameStatus.InProgress)
                return Task.FromResult(
                    CommandResult<CommandEmptyResult>.Fail("You cant add armies while game is in progress"));

            var army = new Army
            {
                Id = Guid.NewGuid(),
                AttackStrategy = request.AttackStrategy,
                Name = request.Name,
                Events = new List<Event>()
            };

            activeGame.Armies.Add(army);

            var armyAddedEvent = new Event
            {
                Id = Guid.NewGuid(),
                EventType = EventType.ArmyCreated,
                Value = request.Units.ToString(),
                GameTime = new TimeSpan(0, 0, 0)
            };

            army.Events.Add(armyAddedEvent);

            return Task.FromResult(CommandResult<CommandEmptyResult>.Success(new CommandEmptyResult()));
        }
    }
}
