﻿using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data;
using Hangfire;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BattleGame.Domain.Services
{
    public class BattleSimulator
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Game> _gameRepository;
        private Game ActiveGame { get; set; }

        private TimeSpan GameTime;
        private string JobId;
        private const long TicksPerSecond = 10000000;

        public BattleSimulator(IRepository<Game> gameRepository, IUnitOfWork unitOfWork)
        {
            _gameRepository = gameRepository;
            _unitOfWork = unitOfWork;

        }

        public void StartGame()
        {
            ActiveGame = _gameRepository.QueryAll()
                .Include(x => x.Armies)
                .ThenInclude(x => x.Events).GetActiveGame();
            GameTime = new TimeSpan(0, 0, 0);
            ActiveGame.Initialize();
            ActiveGame.GameStatus = GameStatus.InProgress;
            JobId = BackgroundJob.Schedule(() => Advance(ActiveGame.NextAttackIn), DateTime.Now);
            _unitOfWork.SaveChanges();
        }

        public void ContinueGame()
        {
            ActiveGame = _gameRepository.QueryAll()
                .Include(x => x.Armies)
                .ThenInclude(x => x.Events).GetActiveGame();
            if (ActiveGame.GameStatus == GameStatus.InProgress)
            {
                ActiveGame.Initialize();
                GameTime = ActiveGame.Armies.Max(x => x.Events.Max(e => e.GameTime));
                JobId = BackgroundJob.Schedule(() => Advance(ActiveGame.NextAttackIn), DateTime.Now);
            }
        }

        public void Advance(decimal timePassed)
        {
            if (ActiveGame == null)
            {
                return;
            }
            GameTime = GameTime.Add(new TimeSpan((long)(timePassed * TicksPerSecond)));

            ActiveGame.NextTurn(timePassed, GameTime);

            BackgroundJob.Delete(JobId);
            if (ActiveGame.Armies.Alive().Count() == 1)
            {
                ActiveGame.GameStatus = GameStatus.Finished;
            }
            else
            {
                BackgroundJob.Schedule(() => Advance(ActiveGame.NextAttackIn), DateTime.Now.AddSeconds((double)timePassed));
            }

            _unitOfWork.SaveChanges();

        }

        public void ResetGame()
        {
            BackgroundJob.Delete(JobId);
            ActiveGame.Reset(GameTime);
            BackgroundJob.Schedule(() => Advance(ActiveGame.NextAttackIn), new TimeSpan(0, 0, 0));
        }
    }
}
