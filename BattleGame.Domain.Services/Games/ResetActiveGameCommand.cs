﻿using BattleGame.Domain.Core;
using MediatR;

namespace BattleGame.Domain.Services.Games
{
    public class ResetActiveGameCommand : IRequest<CommandResult<CommandEmptyResult>>
    {
    }
}
