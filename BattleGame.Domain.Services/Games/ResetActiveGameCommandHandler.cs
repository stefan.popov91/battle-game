﻿using BattleGame.Domain.Core;
using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BattleGame.Domain.Services.Games
{
    public class ResetActiveGameCommandHandler : IRequestHandler<ResetActiveGameCommand, CommandResult<CommandEmptyResult>>
    {
        private readonly BattleSimulator _battleSimulator;
        private readonly IRepository<Game> _gameRepository;

        public ResetActiveGameCommandHandler(BattleSimulator battleSimulator, IRepository<Game> gameRepository)
        {
            _battleSimulator = battleSimulator;
            _gameRepository = gameRepository;
        }

        public Task<CommandResult<CommandEmptyResult>> Handle(ResetActiveGameCommand request, CancellationToken cancellationToken)
        {
            var activeGame = _gameRepository.QueryAll().GetActiveGame();
            if (activeGame == null || activeGame.GameStatus != GameStatus.InProgress)
            {
                return Task.FromResult(CommandResult<CommandEmptyResult>.Fail("There is no game in progress."));
            }

            _battleSimulator.ResetGame();
            return Task.FromResult(CommandResult<CommandEmptyResult>.Success(new CommandEmptyResult()));
        }
    }
}
