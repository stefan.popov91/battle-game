﻿using MediatR;
using System;
using System.Collections.Generic;

namespace BattleGame.Domain.Services.Games
{
    public class RetrieveGameLogCommand : IRequest<CommandResult<ICollection<string>>>
    {
        public Guid GameId { get; set; }
    }
}
