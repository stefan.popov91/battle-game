﻿using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BattleGame.Domain.Services.Games
{
    public class RetrieveGameLogCommandHandler : IRequestHandler<RetrieveGameLogCommand, CommandResult<ICollection<string>>>
    {
        private readonly IRepository<Game> _gameRepository;

        public RetrieveGameLogCommandHandler(IRepository<Game> gameRepository)
        {
            _gameRepository = gameRepository;
        }

        public Task<CommandResult<ICollection<string>>> Handle(RetrieveGameLogCommand request, CancellationToken cancellationToken)
        {
            var game = _gameRepository.QueryAll()
                .Include(x => x.Armies)
                .ThenInclude(x => x.Events)
                .ThenInclude(x => x.Army)
                .Single(x => x.Id == request.GameId);

            var log = game.GetAllEvents().Select(x => x.LogEntry).ToList();

            return Task.FromResult(CommandResult<ICollection<string>>.Success(log));
        }
    }
}
