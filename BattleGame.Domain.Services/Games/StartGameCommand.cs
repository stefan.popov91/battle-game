﻿using MediatR;

namespace BattleGame.Domain.Services.Games
{
    public class StartGameCommand : IRequest<CommandResult<string>>
    {
    }
}
