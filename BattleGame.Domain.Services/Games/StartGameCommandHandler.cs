﻿using System.Threading;
using System.Threading.Tasks;
using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BattleGame.Domain.Services.Games
{
    public class StartGameCommandHandler : IRequestHandler<StartGameCommand, CommandResult<string>>
    {
        private readonly BattleSimulator _battleSimulator;
        private readonly IRepository<Game> _gameRepository;

        public StartGameCommandHandler(BattleSimulator battleSimulator, IRepository<Game> gameRepository)
        {
            _battleSimulator = battleSimulator;
            _gameRepository = gameRepository;
        }

        public Task<CommandResult<string>> Handle(StartGameCommand request, CancellationToken cancellationToken)
        {
            var activeGame = _gameRepository.QueryAll()
                .Include(x => x.Armies)
                .ThenInclude(x => x.Events).GetActiveGame();
            if (activeGame == null || activeGame.GameStatus == GameStatus.InProgress)
            {
                return Task.FromResult(CommandResult<string>.Fail("Game is either in progress or not yet created."));
            }

            if (activeGame.Armies.Count < 10)
            {
                return Task.FromResult(CommandResult<string>.Fail("There needs to be at least 10 armies in order to start a game."));
            }

            _battleSimulator.StartGame();

            return Task.FromResult(CommandResult<string>.Success("Game started"));
        }
    }
}
