﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BattleGame.Domain.Model;

namespace BattleGame.Infrastructure.Data
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> QueryAll();
        IQueryable<T> QueryAllNoTracking();
        void Add(T item);
        void Delete(T item);
        void Delete(Guid id);
        T FindById(Guid id);
        Task<T> FindByIdAsync(Guid id);
        void DeleteRange(IEnumerable<T> entities);
    }
}
