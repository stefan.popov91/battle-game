﻿using System.Threading;
using System.Threading.Tasks;

namespace BattleGame.Infrastructure.Data
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        int SaveChanges();
        void CancelSaving();
    }
}
