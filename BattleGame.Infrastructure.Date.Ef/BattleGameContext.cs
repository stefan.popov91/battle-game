﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BattleGame.Domain.Model;
using BattleGame.Infrastructure.Data.Ef.Configuration;
using Microsoft.EntityFrameworkCore;

namespace BattleGame.Infrastructure.Data.Ef
{
    public class BattleGameContext : DbContext
    {
        public Action<ModelBuilder> ModelBuilderConfigurator { private get; set; }

        public BattleGameContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var a = typeof(GameConfiguration).Assembly;
            modelBuilder.UseEntityTypeConfiguration(typeof(GameConfiguration).Assembly);

            RegisterEntities(modelBuilder);

            ModelBuilderConfigurator?.Invoke(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void RegisterEntities(ModelBuilder modelBuilder)
        {
            MethodInfo entityMethod = typeof(ModelBuilder).GetMethods().First(m => m.Name == "Entity" && m.IsGenericMethod);

            IEnumerable<Type> entityTypes = Assembly.GetAssembly(typeof(Game)).GetTypes()
                .Where(x => (x.IsSubclassOf(typeof(Entity)) && !x.IsAbstract));

            foreach (Type type in entityTypes)
            {
                entityMethod.MakeGenericMethod(type).Invoke(modelBuilder, new object[] { });
            }
        }
    }
}
