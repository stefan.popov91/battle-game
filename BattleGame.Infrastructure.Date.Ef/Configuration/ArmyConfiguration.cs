﻿using BattleGame.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BattleGame.Infrastructure.Data.Ef.Configuration
{
    public class ArmyConfiguration : IEntityTypeConfiguration<Army>
    {
        public void Configure(EntityTypeBuilder<Army> builder) 
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.AttackStrategy).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Ignore(x => x.Units);
            builder.Ignore(x => x.NextAttackIn);

            builder.HasOne(x => x.Game).WithMany(x => x.Armies).HasForeignKey(x => x.GameId);
            builder.HasMany(x => x.Events).WithOne(x => x.Army).HasForeignKey(x => x.ArmyId);
        }
    }
}
