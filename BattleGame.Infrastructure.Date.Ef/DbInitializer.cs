﻿using Microsoft.EntityFrameworkCore;

namespace BattleGame.Infrastructure.Data.Ef
{
    public static class DbInitializer
    {
        public static void Initialize(BattleGameContext context)
        {
            context.Database.Migrate();
            context.SaveChanges();
        }
    }
}
