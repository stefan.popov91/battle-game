﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BattleGame.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace BattleGame.Infrastructure.Data.Ef
{
    public class EntityFrameworkRepository<T> : IRepository<T> where T : Entity
    {
        protected DbContext DbContext { get; }
        protected DbSet<T> DbSet { get; }

        public EntityFrameworkRepository(DbContext context)
        {
            DbContext = context;
            DbSet = context.Set<T>();
        }

        public IQueryable<T> QueryAll()
        {
            return DbSet;
        }
       
        public IQueryable<T> QueryAllNoTracking()
        {
            return DbSet.AsNoTracking();
        }
        

        public T FindById(Guid id)
        {
            return DbSet.Find(id);
        }
        public async Task<T> FindByIdAsync(Guid id)
        {
            return await DbSet.FindAsync(id);
        }

        public void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            DbSet.Add(item);
        }
        public void Delete(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            DbSet.Remove(item);
        }

        public void Delete(Guid id)
        {
            T item = FindById(id);
            if (item != null)
            {
                Delete(item);
            }
        }
        public void DeleteRange(IEnumerable<T> entities)
        {
            DbSet.RemoveRange(entities);
        }


    }
}
