﻿using Autofac;
using BattleGame.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using BattleGame.Infrastructure.Data.Ef;

namespace BattleGame.Infrastructure.Autofac
{
    public class EntityFrameworkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.RegisterGeneric(typeof(EntityFrameworkRepository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(IRepository<>).Assembly)
                .Where(
                    type =>
                        ImplementsInterface(typeof(IRepository<>), type) ||
                        type.Name.EndsWith("Repository")
                ).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.Register(CreateDbContextOptions).As<DbContextOptions>().SingleInstance();

            builder.RegisterType<BattleGameContext>().AsSelf().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterType<DbContextUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        private static bool ImplementsInterface(Type interfaceType, Type concreteType)
        {
            return
                concreteType.GetInterfaces().Any(
                    t =>
                        (interfaceType.IsGenericTypeDefinition && t.IsGenericType
                            ? t.GetGenericTypeDefinition()
                            : t) == interfaceType);
        }

        private DbContextOptions CreateDbContextOptions(IComponentContext container)
        {
            var configuration = container.Resolve<IConfiguration>();

            var optionsBuilder = new DbContextOptionsBuilder();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("BattleGameDbContext"),
                sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("BattleGame.Infrastructure.Data.Ef");
                });

            return optionsBuilder.Options;
        }
    }
}
