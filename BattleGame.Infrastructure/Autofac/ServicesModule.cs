﻿using Autofac;
using BattleGame.Domain.Services;
using Hangfire;

namespace BattleGame.Infrastructure.Autofac
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<BattleSimulator>()
                .AsSelf()
                .SingleInstance();
            builder.RegisterType<BackgroundJobClient>().SingleInstance();
        }
    }
}
